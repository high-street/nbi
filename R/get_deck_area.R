#' Get Deck Area
#'
#' Vectorized function to take three inputs from the NBI data and create a 
#' new column for the deck area of the bridge. This new column is used for 
#' many of the national performance measure calculations and is the standard in
#' measuring the size of the bridge relative to others. 
#' 
#' @author Kevin Ford (ford@highstreetconsulting.com)
#' 
#' @param structure_length The name of the column containing 
#' the Structure Length of a Bridge (Source: NBI)
#' @param deck_width_out_to_out The name of the column containing 
#' the Deck Width of a Bridge (Source: NBI)
#' @param approach_roadway_width The name of the column containing 
#' the approach Width of a Bridge (Source: NBI)
#' @param units The units of the measurements
#' @param decimal_place The number of decimals the data goes out to
#' 
#' 
#' @return Function returns: A vector of deck areas based on 

get_deck_area <- function(
  structure_length, 
  deck_width_out_to_out, 
  approach_roadway_width, 
  units = "Meters", 
  decimal_place = "Tenths"
  ) {
  
  # Vectorize
  structure_length  <-  {{ structure_length }}
  deck_width_out_to_out <- {{ deck_width_out_to_out }}
  approach_roadway_width <- {{ approach_roadway_width }}
  
  # Assume length and width measurements all reported with the same number 
  # of decimal places and units
  # NBI data is reported in XXX.X with a presumed decimal point so must correct
  if (decimal_place == "Tenths") {
    structure_length  <-  structure_length / 10
    deck_width_out_to_out <- deck_width_out_to_out / 10
    approach_roadway_width <- approach_roadway_width / 10
  }
  
  # Convert to feet using: 100 cm = 1 m, 2.54 in = 1 cm, 12 in = 1 ft
  if (units == "Meters") {
    structure_length <- structure_length * 100 / 2.54 / 12
    deck_width_out_to_out <- deck_width_out_to_out * 100 / 2.54 / 12
    approach_roadway_width <- approach_roadway_width * 100 / 2.54 / 12
  }
  
  width <- deck_width_out_to_out
  width <- ifelse(is.na(width) | width == 0, approach_roadway_width, width)
  width <- ifelse(is.na(width) | width == 0, 0, width)
  structure_length <- ifelse(is.na(structure_length), 0, structure_length)
  deck_area <- structure_length * width
  
  return(deck_area)
  
}