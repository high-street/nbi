#' Sum NA Removed
#'
#' Takes the base sum function and defaults na.rm to be TRUE
#' 
#' @author "Rob Owens (owens@hightstreetconsutling.com)"
#'
#' @param x A numeric vector to be summed
#' @return numeric value.

sum_na <- function(x) {

  x <- sum(x, na.rm = TRUE)
  return(x)
  
}
