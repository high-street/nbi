#' Evaluate Historical Federal Bridge Performance Measures
#'
#' This function is designed to just give a simple summarized
#' table of historical federal performance. Can be used for historical 
#' performance, calibrating models for forecasting. 
#' 
#' @author Rob Owens \email{owens@@highstreetconsulting.com}
#' 
#' @importFrom dplyr mutate %>% bind_rows summarise group_by select filter
#' 
#' @param nbi_data Provide the data frame produced by the 
#' get_nbi_data() function. 
#' 
#' @return The function returns a summarized data frame 
#' 
#' @export 

get_historical_performance <- function(nbi_data) {
  
  nbi_data <- nbi_data %>%
    mutate(
      is_good = ifelse(rating >= 7, 1, 0),
      is_fair = ifelse(rating %in% c(5, 6), 1, 0),
      is_poor = ifelse(rating <= 4, 1, 0),
    )

    performance_measures <- nbi_data %>%
      filter(is_nhs == 1) %>%
      group_by(year = data_year) %>% 
      # mutate(pct = deck_area / sum(deck_area)) %>% 
      summarise(Good = round(100 * sum(deck_area * is_good, na.rm = TRUE) /
                                   sum(deck_area, na.rm = TRUE), digits = 1),
                Poor = round(100 * sum(deck_area * is_poor, na.rm = TRUE) /
                                   sum(deck_area, na.rm = TRUE), digits = 1)) %>%
      mutate(Fair = 100 - Good - Poor) %>% 
      select(year, Good, Fair, Poor)
      
  return(performance_measures)
  
}
