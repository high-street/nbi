# National Bridge Inventory Analysis Tools #

### What is this repository for? ###

This repository is an R library for all of High Street's functions in relation
to bridge modeling and contains standard functions in order to
pull down data, evaluate condition, and forecast out future performance.

### How do I import these tools into my R session? ###

```r
library(devtools)
devtools::install_bitbucket("high-street/nbi")
```

### Contribution guidelines ###

A review process is required if you are to contribute to this library since
this library may impact multiple scripts and other colleague's processes.

The Master Branch will be used as the production branch for the time being. In
order to make your changes to this branch, please follow the following steps:

#### Steps ####
* Make all your changes on a separate branch. Please make your branch descriptive
and valuable to the reviewers.
`git checkout -b new-branch`
* Make all the code changes you want in the library and commit them to your branch.
(You may have 2+ commits to your branch, that's ok)
* `Code Change Goes Here`
* `git commit -m "Descriptive Comments About Changes"`

**Once you're ready to update the repository**

* Update the documentation of the /man folder with `devtools::document()`
* Ensure the package is compliant by using `devtools::check()`
* Push your branches online to bitbucket.
`git push origin new-branch`
* On bitbucket, you may submit a new "Pull Request", where you should document
all the changes you made as well as make note on whether or not this may change
someone else's project if they are currently using the library and functions. If
so, we may want to explore other solutions, and/or communicate to the rest
of the staff who may be using the library

### Who do I talk to? ###

* If you've found an issue or would like a feature or enhancement, please add too the
issues pages on this repository.
* If you have any questions, please message Rob Owens (owens@highstreetconsulting.com)
* On pull requests please consult [this link](https://www.atlassian.com/git/tutorials/making-a-pull-request)
